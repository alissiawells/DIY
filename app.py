from flask import Flask
from flask import request
from waitress import serve
import json

app = Flask(__name__)

data = pd.read_csv("dataset.csv") 

@app.route("/classify", methods=['POST'])
def predict():
    json = request.get_json()
    text = json["text"] 
    model = joblib.load("model.pkl")
    loaded_vec = CountVectorizer(decode_error="replace",vocabulary=pickle.load(open("feature.pkl", "rb")))
    input_ = loaded_vec.fit_transform(np.array([text]))
    output = str(model.predict(input_))
    return "class: {}".format(output)

@app.route("/")
def hello():
    return "Try postman or curl -d \"@data.json\" -X POST http://0.0.0.0:5000/classify"

if __name__ == "__main__":
    serve(app,host='0.0.0.0', port=5000)
